from django.urls import path
from .views import (
    ShoppingItemListView,
    ShoppingItemCreateView,
    ShoppingItemDeleteView,
    delete_shopping_list,
    add_to_list
)


urlpatterns = [
    path("", ShoppingItemListView.as_view(), name="shopping_list"),
    path("create/", ShoppingItemCreateView.as_view(), name="s_item_create"),
    path(
        "<int:pk>/delete",
        ShoppingItemDeleteView.as_view(),
        name="s_item_delete",
    ),
    path("delete/", delete_shopping_list, name="s_list_delete"),
    path("addbutton/", add_to_list, name = "add_button")
]
