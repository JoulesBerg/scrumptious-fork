from django.shortcuts import redirect, render
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from .models import ShoppingItem
from django.contrib.auth.mixins import LoginRequiredMixin
from recipes.models import FoodItem
from django.http import HttpResponseRedirect




# Create your views here.


class ShoppingItemListView(ListView, LoginRequiredMixin):
    model = ShoppingItem
    context_object_name = "shoppingitems"
    template_name = "shopping_lists/list.html"
    paginate_by = 24

    def get_context_data(
        self,
    ):  # re-define get context data so that pagination and search work together instead of needing to choose between one or the other.
        context = super().get_context_data()
        query = self.request.GET.get("q")
        if not query:
            query = ""
        context[
            "query"
        ] = query  # adds the query from the search box (name q)as a context variable that can be accessed in list.html
        return context

    def get_queryset(
        self,
    ):  # re-defining get_queryset to work with search functionality
        user = self.request.user
        query = self.request.GET.get(
            "q"
        )  # q is the name variable defined in the HTML input function in list.html
        if not query:
            query = ""  # ensures that query is not None, as searching for a string that contains None using the filter function below causes an error
        return ShoppingItem.objects.filter(
            food_item__name__icontains=query,  # performs a join of the food_item database column and then searches the name of items in that column
            owner=user,
        )


class ShoppingItemCreateView(LoginRequiredMixin, CreateView):
    model = ShoppingItem
    template_name = "shopping_lists/new.html"
    fields = ["food_item"]

    def form_valid(self, form):
        form.instance.owner = (
            self.request.user
        )  # intercept form to set authoruser automatically

        return super().form_valid(form)

    success_url = reverse_lazy("shopping_list")


class ShoppingItemDeleteView(DeleteView):
    request_method = "POST"
    model = ShoppingItem
    template_name = "shopping_lists/deletesingle.html"
    success_url = reverse_lazy("shopping_list")

    # def form_valid(self, form):  # only owner can update or delete mealplans
    #     item = form.save(commit=False)
    #     if self.request.user == item.owner:
    #         return super().form_valid(form)
    #     else:
    #         return redirect("shopping_list")
    def delete(self, request, *args, **kwargs):
        if request.user == self.get_object().owner:
            return super().delete(request,*args,**kwargs)
        else:
            return redirect("shopping_list")

@login_required
def delete_shopping_list(request):
    if request.method == "GET":
        return render(request, "shopping_lists/deleteall.html", {})
    else:
        user = request.user
        for slistitem in user.shopping_items.all():
            # in ShoppingItem.objects.filter(owner=user): would also work
            slistitem.delete()
        return redirect("shopping_list")

def add_to_list(request):
    if(request.GET.get('additem')):
        fooditem = FoodItem.objects.all()
        fpk = int(request.GET.get('food_item_variable'))
        rpk = int(request.GET.get('recipe_item_variable'))
        item = ShoppingItem(food_item = fooditem[fpk-1], owner = request.user )
        item.save()
    return redirect("recipe_detail", pk=rpk)
    
