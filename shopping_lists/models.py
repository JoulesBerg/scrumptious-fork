from django.db import models
from django.conf import settings
from recipes.models import FoodItem

# Create your models here.
class ShoppingItem(models.Model):
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="shopping_items",
        on_delete=models.CASCADE,
    )
    food_item = models.ForeignKey(
        FoodItem,
        related_name="shopping_lists",
        on_delete=models.PROTECT,
        unique=True,
    )
