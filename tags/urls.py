from django.urls import path

from tags.views import (
    TagsListView,
    TagDetailView,
    TagCreateView,
    TagUpdateView,
    TagDeleteView,
)

urlpatterns = [
    path("", TagsListView.as_view(), name="tags_list"),
    path("<int:pk>", TagDetailView.as_view(), name="tag_detail"),
    path("new/", TagCreateView.as_view(), name="tag_new"),
    path("<int:pk>/update", TagUpdateView.as_view(), name="tag_update"),
    path("<int:pk>/delete", TagDeleteView.as_view(), name="tag_delete"),
]
