from django.shortcuts import redirect, render
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
from django.urls import reverse_lazy

from tags.models import Tag


# Create your views here.
def show_tags(request):
    context = {
        "tags": Tag.objects.all() if Tag else None,
    }
    return render(request, "tags/list.html", context)


class TagsListView(ListView):
    model = Tag
    context_object_name = "tags"
    template_name = "tags/list.html"
    paginate_by = 56

    def get_context_data(
        self,
    ):  # re-define get context data so that pagination and search work together instead of needing to choose between one or the other.
        context = super().get_context_data()
        query = self.request.GET.get("q")
        if not query:
            query = ""
        context[
            "query"
        ] = query  # adds the query from the search box (name q)as a context variable that can be accessed in list.html
        return context

    def get_queryset(
        self,
    ):  # re-defining get_queryset to work with search functionality
        query = self.request.GET.get(
            "q"
        )  # q is the name variable defined in the HTML input function in list.html
        if not query:
            query = ""  # ensures that query is not None, as searching for a string that contains None using the filter function below causes an error
        return Tag.objects.filter(name__icontains=query)


class TagDetailView(DetailView):
    model = Tag
    context_object_name = "tag"
    template_name = "tags/detail.html"


class TagCreateView(CreateView):
    model = Tag
    fields = ["name", "description"]
    template_name = "tags/new.html"
    success_url = reverse_lazy("tags_list")

    def form_valid(self, form):  # only staff can add/update/or delete tags
        if self.request.user.is_staff:
            return super().form_valid(form)
        else:
            return redirect("tags_list")


class TagUpdateView(UpdateView):
    model = Tag
    fields = ["name", "description"]
    template_name = "tags/edit.html"
    success_url = reverse_lazy("tag_list")

    def form_valid(self, form):  # only staff can add/update/or delete tags
        if self.request.user.is_staff:
            return super().form_valid(form)
        else:
            return redirect("tag_detail")


class TagDeleteView(DeleteView):
    model = Tag
    template_name = "tags/delete.html"
    success_url = reverse_lazy("tags_list")

    def form_valid(self, form):  # only staff can add/update/or delete tags
        if self.request.user.is_staff:
            return super().form_valid(form)
        else:
            return redirect("tag_detail")
