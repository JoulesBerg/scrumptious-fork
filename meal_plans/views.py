from django.shortcuts import redirect, render
from meal_plans.models import MealPlan
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)

# Create your views here.
class MealPlanCreateView(LoginRequiredMixin, CreateView):
    model = MealPlan
    template_name = "meal_plans/new.html"
    fields = [
        "name",
        "date",
        "recipes",
    ]

    def form_valid(self, form):
        form.instance.owner = (
            self.request.user
        )  # intercept form to set authoruser automatically

        return super().form_valid(form)

    success_url = reverse_lazy("mealplans")


class MealPlanUpdateView(LoginRequiredMixin, UpdateView):
    model = MealPlan
    template_name = "meal_plans/edit.html"
    fields = [
        "name",
        "date",
        "recipes",
    ]
    success_url = reverse_lazy("mealplan_detail")
    def form_valid(self, form):  # only owner can update or delete mealplans
        if self.request.user == MealPlan.owner:
            return super().form_valid(form)
        else:
            return redirect("mealplan_detail")


class MealPlanListView(LoginRequiredMixin, ListView):
    model = MealPlan
    context_object_name = "mealplans"
    template_name = "meal_plans/list.html"
    paginate_by = 12

    def get_context_data(
        self,
    ):  # re-define get context data so that pagination and search work together instead of needing to choose between one or the other.
        context = super().get_context_data()
        query = self.request.GET.get("q")
        if not query:
            query = ""
        context[
            "query"
        ] = query  # adds the query from the search box (name q)as a context variable that can be accessed in list.html
        return context

    def get_queryset(
        self,
    ):  # re-defining get_queryset to work with search functionality
        query = self.request.GET.get(
            "q"
        )  # q is the name variable defined in the HTML input function in list.html
        if not query:
            query = ""  # ensures that query is not None, as searching for a string that contains None using the filter function below causes an error
        return MealPlan.objects.filter(name__icontains=query)


class MealPlanDetailView(LoginRequiredMixin, DetailView):
    model = MealPlan
    context_object_name = "mealplan"
    template_name = "meal_plans/detail.html"
    


class MealPlanDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlan
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("mealplans")
    def form_valid(self, form):  # only owner can update or delete mealplans
        if self.request.user == MealPlan.owner:
            return super().form_valid(form)
        else:
            return redirect("mealplan_detail")
