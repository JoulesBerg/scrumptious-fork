from django.contrib import admin
from .models import Recipe, Ingredient, Measure, FoodItem, Step, Rating

# Register your models here.
admin.site.register(Recipe)
admin.site.register(Ingredient)
admin.site.register(Measure)
admin.site.register(FoodItem)
admin.site.register(Step)
admin.site.register(Rating)
