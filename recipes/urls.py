from django.urls import path, include

from recipes.views import (
    RecipeCreateView,
    RecipeUpdateView,
    log_rating,
    RecipesDetailView,
    RecipesListView,
    RecipeDeleteView,
)

urlpatterns = [
    path("", RecipesListView.as_view(), name="recipes_list"),
    path("<int:pk>/", RecipesDetailView.as_view(), name="recipe_detail"),
    path("new/", RecipeCreateView.as_view(), name="recipe_new"),
    path("<int:pk>/edit", RecipeUpdateView.as_view(), name="recipe_edit"),
    path("<int:recipe_id>/ratings/", log_rating, name="recipe_rating"),
    path("<int:pk>/delete", RecipeDeleteView.as_view(), name="recipe_delete"),
    path("shopping_items/", include("shopping_lists.urls")),
]
