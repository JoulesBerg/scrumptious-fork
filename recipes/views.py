from django.shortcuts import redirect, render
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    UpdateView,
    DeleteView,
)
from django.urls import reverse_lazy
from recipes.forms import RatingForm

# from recipes.forms import RecipeForm
from recipes.models import Recipe
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required


# def create_recipe(request):
#     if request.method == "POST":
#         form = RecipeForm(request.POST)
#         if form.is_valid():
#             recipe = form.save()
#             return redirect("recipe_detail", pk=recipe.pk)
#     elif RecipeForm:
#         form = RecipeForm()
#     else:
#         form = None
#     context = {
#         "form": form,
#     }
#     return render(request, "recipes/new.html", context)


class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = [
        "name",
        "description",
        "image",
    ]

    def form_valid(self, form):
        form.instance.author = (
            self.request.user
        )  # intercept form to set authoruser automatically

        return super().form_valid(form)

    success_url = reverse_lazy("recipes_list")


# def change_recipe(request, pk):
#     if Recipe and RecipeForm:
#         instance = Recipe.objects.get(pk=pk)
#         if request.method == "POST":
#             form = RecipeForm(request.POST, instance=instance)
#             if form.is_valid():
#                 form.save()
#                 return redirect("recipe_detail", pk=pk)
#         else:
#             form = RecipeForm(instance=instance)
#     else:
#         form = None
#     context = {
#         "form": form,
#     }
#     return render(request, "recipes/edit.html", context)


class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"
    fields = [
        "name",
        "description",
        "image",
    ]
    success_url = reverse_lazy("recipes_detail")


@login_required  # function view login required syntax
def log_rating(request, recipe_id):
    try:
        if request.method == "POST":
            form = RatingForm(request.POST)
            if form.is_valid():
                rating = form.save(commit=False)
                rating.recipe = Recipe.objects.get(pk=recipe_id)
                rating.save()
                return redirect("recipe_detail", pk=recipe_id)
    except Recipe.DoesNotExist:
        return redirect("recipes_list")



# def show_recipes(request):
#     context = {
#         "recipes": Recipe.objects.all() if Recipe else [],
#     }
#     return render(request, "recipes/list.html", context)


class RecipesListView(ListView):
    model = Recipe
    context_object_name = "recipes"
    template_name = "recipes/list.html"
    paginate_by = 12

    def get_context_data(
        self,
    ):  # re-define get context data so that pagination and search work together instead of needing to choose between one or the other.
        context = super().get_context_data()
        query = self.request.GET.get("q")
        if not query:
            query = ""
        context[
            "query"
        ] = query  # adds the query from the search box (name q)as a context variable that can be accessed in list.html
        return context

    def get_queryset(
        self,
    ):  # re-defining get_queryset to work with search functionality
        query = self.request.GET.get(
            "q"
        )  # q is the name variable defined in the HTML input function in list.html
        if not query:
            query = ""  # ensures that query is not None, as searching for a string that contains None using the filter function below causes an error
        return Recipe.objects.filter(name__icontains=query)


# def show_recipe(request, pk):
#     context = {
#         "recipe": Recipe.objects.get(pk=pk) if Recipe else None,
#         "rating_form": RatingForm(),  # highlight
#     }
#     return render(request, "recipes/detail.html", context)


class RecipesDetailView(DetailView):
    model = Recipe
    context_object_name = "recipe"
    template_name = "recipes/detail.html"
    def get_context_data(self, **kwargs):
            context = super().get_context_data(**kwargs)
            context["rating_form"] = RatingForm()
            foods = []
            for item in self.request.user.shopping_items.all():
                foods.append(item.food_item)
            context["food_in_shopping_list"] = foods
            #try: 
            input_num = self.request.GET.get('multiplier_input')
            if input_num:
                initmultiplier = int(input_num)
                normalservings = context['recipe'].servings
                multiplier = initmultiplier/normalservings
            else:
                multiplier = 1
            #except:
                #print("invalid multiplier process")
                #multiplier = 1
            context['multiplier'] = multiplier
            return context


class RecipeDeleteView(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")
